=======

# spotify-like

### Dev environnement needed :

```bash
    PHP 8.0^'
    node 14^'
```

### Installation process :

```
- composer install
- yarn
```

```
Configure your Database in a .env.local file :
DATABASE_URL="mysql://root@127.0.0.1:3306/spotify-like?serverVersion=5.7"
```

Then create it with this command :
```
symfony console d:d:c
```

Then execute migrations with this command :
```
symfony console d:m:m
```

Run compile asset with :
```
yarn watch
```

Run server with :
```
symfony serve
```