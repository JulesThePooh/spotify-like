<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'Il y a déjà un compte avec ce mail')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 255)]
    private $nickName;

    #[ORM\Column(type: 'string', length: 255)]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    private $LastName;

    #[ORM\Column(type: 'boolean')]
    private $isArtist;

    #[ORM\Column(type: 'boolean')]
    private $canBeAddAsFriend;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $avatarPath;

    #[ORM\ManyToMany(targetEntity: self::class)]
    private $likedArtists;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Subscription::class)]
    private $subscriptions;

    #[ORM\OneToMany(mappedBy: 'receiver', targetEntity: FriendRequest::class)]
    private $friendDemands;

    #[ORM\OneToMany(mappedBy: 'sender', targetEntity: FriendRequest::class)]
    private $friendReceived;

    #[ORM\OneToMany(mappedBy: 'sender', targetEntity: Message::class)]
    private $messageSends;

    #[ORM\OneToMany(mappedBy: 'receiver', targetEntity: Message::class)]
    private $messageReceives;

    #[ORM\OneToMany(mappedBy: 'creator', targetEntity: Album::class)]
    private $albums;

    #[ORM\ManyToMany(targetEntity: Album::class, mappedBy: 'users')]
    private $likedAlbums;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Playlist::class)]
    private $playlists;

    #[ORM\ManyToMany(targetEntity: Song::class, inversedBy: 'users')]
    private $likedSongs;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Appreciation::class)]
    private $appreciations;

    #[ORM\ManyToMany(targetEntity: Genre::class, inversedBy: 'users')]
    private $favoriteGenres;

    public function __construct()
    {
        $this->likedArtists = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->friendDemands = new ArrayCollection();
        $this->friendReceived = new ArrayCollection();
        $this->messageSends = new ArrayCollection();
        $this->messageReceives = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->likedAlbums = new ArrayCollection();
        $this->playlists = new ArrayCollection();
        $this->likedSongs = new ArrayCollection();
        $this->appreciations = new ArrayCollection();
        $this->favoriteGenres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    public function setNickName(string $nickName): self
    {
        $this->nickName = $nickName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->LastName;
    }

    public function setLastName(string $LastName): self
    {
        $this->LastName = $LastName;

        return $this;
    }

    public function getIsArtist(): ?bool
    {
        return $this->isArtist;
    }

    public function setIsArtist(bool $isArtist): self
    {
        $this->isArtist = $isArtist;

        return $this;
    }

    public function getCanBeAddAsFriend(): ?bool
    {
        return $this->canBeAddAsFriend;
    }

    public function setCanBeAddAsFriend(bool $canBeAddAsFriend): self
    {
        $this->canBeAddAsFriend = $canBeAddAsFriend;

        return $this;
    }

    public function getAvatarPath(): ?string
    {
        return $this->avatarPath;
    }

    public function setAvatarPath(?string $avatarPath): self
    {
        $this->avatarPath = $avatarPath;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getLikedArtists(): Collection
    {
        return $this->likedArtists;
    }

    public function addLikedArtist(self $likedArtist): self
    {
        if (!$this->likedArtists->contains($likedArtist)) {
            $this->likedArtists[] = $likedArtist;
        }

        return $this;
    }

    public function removeLikedArtist(self $likedArtist): self
    {
        $this->likedArtists->removeElement($likedArtist);

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setUser($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): self
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getUser() === $this) {
                $subscription->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FriendRequest>
     */
    public function getFriendDemands(): Collection
    {
        return $this->friendDemands;
    }

    public function addFriendDemand(FriendRequest $friendDemand): self
    {
        if (!$this->friendDemands->contains($friendDemand)) {
            $this->friendDemands[] = $friendDemand;
            $friendDemand->setReceiver($this);
        }

        return $this;
    }

    public function removeFriendDemand(FriendRequest $friendDemand): self
    {
        if ($this->friendDemands->removeElement($friendDemand)) {
            // set the owning side to null (unless already changed)
            if ($friendDemand->getReceiver() === $this) {
                $friendDemand->setReceiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FriendRequest>
     */
    public function getFriendReceived(): Collection
    {
        return $this->friendReceived;
    }

    public function addFriendReceived(FriendRequest $friendReceived): self
    {
        if (!$this->friendReceived->contains($friendReceived)) {
            $this->friendReceived[] = $friendReceived;
            $friendReceived->setSender($this);
        }

        return $this;
    }

    public function removeFriendReceived(FriendRequest $friendReceived): self
    {
        if ($this->friendReceived->removeElement($friendReceived)) {
            // set the owning side to null (unless already changed)
            if ($friendReceived->getSender() === $this) {
                $friendReceived->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessageSends(): Collection
    {
        return $this->messageSends;
    }

    public function addMessageSend(Message $messageSend): self
    {
        if (!$this->messageSends->contains($messageSend)) {
            $this->messageSends[] = $messageSend;
            $messageSend->setSender($this);
        }

        return $this;
    }

    public function removeMessageSend(Message $messageSend): self
    {
        if ($this->messageSends->removeElement($messageSend)) {
            // set the owning side to null (unless already changed)
            if ($messageSend->getSender() === $this) {
                $messageSend->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessageReceives(): Collection
    {
        return $this->messageReceives;
    }

    public function addMessageReceife(Message $messageReceife): self
    {
        if (!$this->messageReceives->contains($messageReceife)) {
            $this->messageReceives[] = $messageReceife;
            $messageReceife->setReceiver($this);
        }

        return $this;
    }

    public function removeMessageReceife(Message $messageReceife): self
    {
        if ($this->messageReceives->removeElement($messageReceife)) {
            // set the owning side to null (unless already changed)
            if ($messageReceife->getReceiver() === $this) {
                $messageReceife->setReceiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setCreator($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            // set the owning side to null (unless already changed)
            if ($album->getCreator() === $this) {
                $album->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getLikedAlbums(): Collection
    {
        return $this->likedAlbums;
    }

    public function addLikedAlbum(Album $likedAlbum): self
    {
        if (!$this->likedAlbums->contains($likedAlbum)) {
            $this->likedAlbums[] = $likedAlbum;
            $likedAlbum->addUser($this);
        }

        return $this;
    }

    public function removeLikedAlbum(Album $likedAlbum): self
    {
        if ($this->likedAlbums->removeElement($likedAlbum)) {
            $likedAlbum->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Playlist>
     */
    public function getPlaylists(): Collection
    {
        return $this->playlists;
    }

    public function addPlaylist(Playlist $playlist): self
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists[] = $playlist;
            $playlist->setUser($this);
        }

        return $this;
    }

    public function removePlaylist(Playlist $playlist): self
    {
        if ($this->playlists->removeElement($playlist)) {
            // set the owning side to null (unless already changed)
            if ($playlist->getUser() === $this) {
                $playlist->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Song>
     */
    public function getLikedSongs(): Collection
    {
        return $this->likedSongs;
    }

    public function addLikedSong(Song $likedSong): self
    {
        if (!$this->likedSongs->contains($likedSong)) {
            $this->likedSongs[] = $likedSong;
        }

        return $this;
    }

    public function removeLikedSong(Song $likedSong): self
    {
        $this->likedSongs->removeElement($likedSong);

        return $this;
    }

    /**
     * @return Collection<int, Appreciation>
     */
    public function getAppreciations(): Collection
    {
        return $this->appreciations;
    }

    public function addAppreciation(Appreciation $appreciation): self
    {
        if (!$this->appreciations->contains($appreciation)) {
            $this->appreciations[] = $appreciation;
            $appreciation->setUser($this);
        }

        return $this;
    }

    public function removeAppreciation(Appreciation $appreciation): self
    {
        if ($this->appreciations->removeElement($appreciation)) {
            // set the owning side to null (unless already changed)
            if ($appreciation->getUser() === $this) {
                $appreciation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Genre>
     */
    public function getFavoriteGenres(): Collection
    {
        return $this->favoriteGenres;
    }

    public function addFavoriteGenre(Genre $favoriteGenre): self
    {
        if (!$this->favoriteGenres->contains($favoriteGenre)) {
            $this->favoriteGenres[] = $favoriteGenre;
        }

        return $this;
    }

    public function removeFavoriteGenre(Genre $favoriteGenre): self
    {
        $this->favoriteGenres->removeElement($favoriteGenre);

        return $this;
    }

}
