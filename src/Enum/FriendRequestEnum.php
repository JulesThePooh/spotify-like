<?php

namespace App\Enum;

class FriendRequestEnum
{
    const PENDING = 100;
    const ACCEPTED = 200;
    const DECLINED = 300;
    const DELETED = 400;
}