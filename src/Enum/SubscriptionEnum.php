<?php

namespace App\Enum;

class SubscriptionEnum
{
    const PAYED = 200;
    const FAILED = 300;
    const IN_TREATMENT = 100;
}